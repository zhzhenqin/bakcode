@echo off
@setlocal enableextensions enabledelayedexpansion
set MAINCLASS=com.sdyc.clstest.t.HelloWorld
set LIB_DIR=lib
set CLASS_DIR=classes
set PRO_ARGS=
set JVM_ARGS=-Xmx128m

set jcommand=java.exe
set CURRENT_DIR=%~dp0
set runtype=-classpath
set LIB_DIR=%CURRENT_DIR%\%LIB_DIR%
set CLASSES_DIR=%CURRENT_DIR%\%CLASS_DIR%
set APPEND_LIB=%CLASSES_DIR%
set FILE_SUFFIX=*.jar

@echo CURRENT_DIR=%CURRENT_DIR%
@echo off

for %%a in (%LIB_DIR%\%FILE_SUFFIX%) do (
    set APPEND_LIB=!APPEND_LIB!;%%a
)
%jcommand% %JVM_ARGS% %runtype% %APPEND_LIB% %MAINCLASS% %PRO_ARGS%