#!/usr/bin/env bash

#JAVA_HOME=/opt/jdk1.7.0_07
MAINCLASS="com.sdyc.clstest.t.HelloWorld"
LIB_DIR="lib"
CLASS_DIR="classes"
PRO_ARGS=""
JVM_ARGS="-Xmx128m"


jcommand="java"
current_dir=$(cd "$(dirname "$0")"; pwd)
runtype="-classpath"
APPEND_LIB=$current_dir"/"$CLASS_DIR

echo "CURRENT_DIR="$current_dir

#注意这个字符,不是',是`,数字键左边的那个键
filelist=`ls $current_dir"/"$LIB_DIR`
for f in $filelist
  do
    name=$current_dir"/"$LIB_DIR"/"$f
    APPEND_LIB=$APPEND_LIB:$name
  done

exec $jcommand $JVM_ARGS $runtype $APPEND_LIB $MAINCLASS $PRO_ARGS
