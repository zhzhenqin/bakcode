
'''
Created on 2013-1-25

@author: ZhenQin
'''

import os
import platform

MAIN_CLASS="com.sdyc.clstest.t.HelloWorld"
DEFAULT_LIB="lib"
DEFAULT_CP="classes"

JVM_ARGS="-Xmx128m"
PRO_ARGS=""


SYSTEM="Windows"
FILESEP="\\"
SPLIT=";"
sysstr = platform.system()
if(sysstr =="Windows"):
    SYSTEM="Windows"
    FILESEP="\\"
    SPLIT=";"
elif(sysstr == "Linux"):
    SYSTEM="Linux"
    FILESEP="/"
    SPLIT=":"
else:
    SYSTEM="Linux"
    FILESEP="/"
    SPLIT=":"

def run(mainRun = "java"):
    return mainRun

def jvmArgs(vmArgs = ""):
    vmArgs += JVM_ARGS
    return vmArgs;

def classpath(classpath = ""):
    currentDir = os.getcwd()
    libdir = currentDir + "/" + DEFAULT_LIB;
    libfile = currentDir + FILESEP + DEFAULT_LIB;
    clazzdir = currentDir + FILESEP + DEFAULT_CP;
    if classpath != "" :
        classpath = classpath + SPLIT + clazzdir;
    else :
        classpath = clazzdir;
        
    for fileordir in os.listdir(libdir):
        if os.path.isfile(libdir + "/" + fileordir):
            classpath = classpath + SPLIT + libfile + FILESEP + fileordir
            
    return classpath


def mainClass():
    return MAIN_CLASS;

def args(args = ""):
    return args + PRO_ARGS;

if __name__ == '__main__':
    run_arg = run() + " " + jvmArgs();
    if classpath() != "" :
        run_arg += (" -classpath " + classpath());
    run_arg += (" " + mainClass() + " " + args())
    print "CONSOLE_EXECUTE: \n" + run_arg
    result = os.popen(run_arg).readlines()
    print result
